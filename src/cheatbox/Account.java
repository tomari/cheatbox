package cheatbox;

public class Account 
{
	private String famName;
	private String indName;
	private int studentId;
	private String email;
	public void setFamName(String famName) { this.famName=famName; }
	public String getFamName() { return this.famName; }
	public void setIndName(String indName) { this.indName=indName; }
	public String getIndName() { return this.indName; }
	public void setStudentId(int studentId) { this.studentId=studentId; }
	public int getStudentId() { return this.studentId; }
	public void setEmail(String email) { this.email=email; }
	public String getEmail() { return this.email; }
}
