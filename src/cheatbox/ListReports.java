package cheatbox;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.naming.*;
import java.sql.*;
import javax.sql.*;

public class ListReports extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		doGet(request,response);
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		HttpSession session=request.getSession(false);
		Account user;
		if(session==null) {
			return;
		} else if((user=(Account)session.getAttribute("account"))==null) {
			return;
		}
		Connection cn=null;
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try{
			InitialContext cxt = new InitialContext();
			DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgres" );
			cn=ds.getConnection();
			stmt=cn.prepareStatement("select report,MAX(submit_time) from submission where s_id= ? group by report order by report");
			stmt.setInt(1,user.getStudentId());
			rs=stmt.executeQuery();
			PrintWriter out=response.getWriter();
			while(rs.next()) {
				out.println("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getString(2)+"</td></tr>");
			}
		} catch (Exception e) {
			;
		} finally {
			try {
				if(rs!=null)
					rs.close();
				if(stmt!=null)
					stmt.close();
				if(cn!=null)
					cn.close();
			} catch (SQLException e) {
				;
			}
		}
	}

}
