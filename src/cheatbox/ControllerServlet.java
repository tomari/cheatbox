package cheatbox;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.naming.*;
import java.sql.*;
import javax.sql.*;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import java.util.*;

public class ControllerServlet extends HttpServlet {
	private String storagepath;
	public void init() {
		storagepath=getInitParameter("storagePath");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		doGet(request,response);
	}

	/* authenticate user */
	private Account authenticate(int name, String password)
		throws NamingException, SQLException
	{
		InitialContext cxt = new InitialContext();
		DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgres" );
		Connection cn=null;
		PreparedStatement stmt=null;
		ResultSet rs=null;
		Account a;
		try {
			cn=ds.getConnection();
			stmt=cn.prepareStatement("SELECT famname,indname,s_id,email FROM users WHERE s_id= ? AND PASSWORD=md5( ? )");
			stmt.setInt(1,name);
			stmt.setString(2,password);
			rs=stmt.executeQuery();
			rs.next();
			a=new Account();
			a.setFamName(rs.getString(1));
			a.setIndName(rs.getString(2));
			a.setStudentId(rs.getInt(3));
			a.setEmail(rs.getString(4));
		} finally {
			if(rs!=null)
				rs.close();
			if(stmt!=null)
				stmt.close();
			if(cn!=null)
				cn.close();
		}
		return a;
	}

	private void authfail(HttpServletRequest request, HttpServletResponse response, String username)
		throws IOException, ServletException
	{
		RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
		LoginRequest lr=new LoginRequest();
		lr.setUserName(username);
		String failstring=request.getParameter("failcount");
		lr.setFailCount((failstring==null)?0:(1+Integer.valueOf(failstring)));
		request.setAttribute("req",lr);
		rd.forward(request,response);
	}
	/* error 9=database fail 1=update success */
	private int passwd(int s_id, String oldpass, String newpass) {
		int res=9;
		Connection cn=null;
		PreparedStatement stmt=null;
		try {
			InitialContext cxt = new InitialContext();
			DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgres" );
			cn=ds.getConnection();
			stmt=cn.prepareStatement("UPDATE users SET password=md5( ? ), reg_time=now() WHERE s_id= ? AND password=md5( ? )");
			stmt.setString(1,newpass);
			stmt.setInt(2,s_id);
			stmt.setString(3,oldpass);
			int rows=stmt.executeUpdate();
			if(rows==1)
				res=1;
			else
				res=2;
		} catch (Exception e) {
			res=9;
		} finally {
			try {
				if(stmt!=null)
					stmt.close();
				if(cn!=null)
					cn.close();
			} catch (SQLException e) {
			}
		}
		return res;
	}

	private void accept_submission(HttpServletRequest request, HttpServletResponse response, Account user)
		throws Exception
	{
		int reportnum=99;
		/* parse arg */
		DiskFileItemFactory factory=new DiskFileItemFactory();
		ServletFileUpload upload=new ServletFileUpload(factory);
		factory.setSizeThreshold(10*1024*1024);
		upload.setSizeMax(5*1024*1024);
		upload.setHeaderEncoding("utf-8");
		List list=upload.parseRequest(request);
		Iterator i=list.iterator();
		FileItem savefile=null;
		while(i.hasNext()) {
			FileItem itm=(FileItem)i.next();
			if(!itm.isFormField()) {
				savefile=itm;
			} else if(itm.getFieldName().equals("repno")) {
				reportnum=Integer.valueOf(itm.getString());
			}
		}
		if(reportnum<0 || reportnum>19)
			throw new Exception("Illegal Report Number");

		/* Save file */
		String pathname=savefile.getName();
		if((pathname!=null) && !pathname.equals("")) {
			pathname=storagepath+"/"+(user.getStudentId())+"-"+reportnum+"-"+(new File(pathname)).getName();
		}
		savefile.write(new File(pathname));

		/* update database */
		Connection cn=null;
		PreparedStatement stmt=null;
		try {
			InitialContext cxt = new InitialContext();
			DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgres" );
			cn=ds.getConnection();
			stmt=cn.prepareStatement("INSERT INTO submission (s_id, report, submit_time) VALUES ( ? , ? , now())");
			stmt.setInt(1,user.getStudentId());
			stmt.setInt(2,reportnum);
			int rows=stmt.executeUpdate();
		} finally {
			if(stmt!=null)
				stmt.close();
			if(cn!=null)
				cn.close();
		}
 	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{

		String rqs=(new File(request.getRequestURI())).getName();
		if(rqs.equals("login.do")) { /* Login request */
			/* already logged in? */
			HttpSession session=request.getSession(false);
			try{
				Account a=(Account)session.getAttribute("account");
				if(a.getFamName().length()>0) {
					RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
					rd.forward(request,response);
					return;
				}
			} catch (Exception e) {
				;
			}
			String username=request.getParameter("namae");
			String password=request.getParameter("pass");
			Account acc;
			try {
				acc=authenticate(Integer.valueOf(username),password);
			} catch(Exception e) {
				acc=null;
			}
			if(acc!=null) { /* login success -> main menu */
				session=request.getSession(true);
				session.setAttribute("account",acc);
				RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
				rd.forward(request,response);
			} else { /* login fail */
				authfail(request,response,username);
				return;
			}
		} else {
			/* have to be logged in to proceed to following functions */
			HttpSession session=request.getSession(false);
			Account user;
			if(session==null) {
				authfail(request,response,"");
				return;
			} else if((user=(Account)session.getAttribute("account"))==null) {
				authfail(request,response,"");
				return;
			}

			if(rqs.equals("logout.do")) {
				session.invalidate();
				RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
				rd.forward(request,response);
			} else if(rqs.equals("passwd.do")) {
				String oldpass=request.getParameter("oldpass");
				String newpass1=request.getParameter("pass1");
				String newpass2=request.getParameter("pass2");
				PasswdRequest pr=new PasswdRequest();
				if(!newpass1.equals(newpass2))
					pr.setStatus(2);
				else
					pr.setStatus(passwd(user.getStudentId(), oldpass,newpass1));
				RequestDispatcher rd=request.getRequestDispatcher("passwd.jsp");
				request.setAttribute("preq",pr);
				rd.forward(request,response);
				return;
			} else if(rqs.equals("submit.do")) {
				/* file upload */
				try {
					accept_submission(request,response,user);
				} catch (Exception e) {
					response.sendRedirect("submit_ng.html");
					return;
				}
				response.sendRedirect("submit_ok.html");
				return;

			} else {
				response.sendRedirect("index.jsp");
			}
		}
	}
}
