package cheatbox;

public class LoginRequest
{
	private String userName;
	private int failCount = 0;
	public void setUserName(String userName) { this.userName=userName; }
	public String getUserName() { return this.userName; }
	public void setFailCount(int failCount) { this.failCount=failCount; }
	public int getFailCount() { return this.failCount; }
}
